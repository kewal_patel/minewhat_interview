import sys
import operator

def EditDistance(s1,s2):
    if len(s1) > len(s2):
        s1,s2 = s2,s1
    distances = range(len(	s1) + 1)
    for index2,char2 in enumerate(s2):
        newDistances = [index2+1]
        for index1,char1 in enumerate(s1):
            if char1 == char2:
                newDistances.append(distances[index1])
            else:
                newDistances.append(1 + min((distances[index1],
                                             distances[index1+1],
                                             newDistances[-1])))
        distances = newDistances
    return distances[-1]

def NumericDistance(n1,n2):
	if n2 > n1:
		return n2 - n1
	else:
		return n1 - n2

def print_output(result_dict, closest_num):
	for key,value in result_dict.items():
		print "The closest {0} items for key {1} are ----> {2}".format(closest_num,key, [word for (distance,word,length) in value])

def main():
	if len(sys.argv) == 3:
		file_name = sys.argv[1]
		closest_num = int(sys.argv[2])

	with open(file_name,'r') as file:
		content = file.read().split()
		result_dict = dict()
		for string in content:
			result_list = list()
			for word in content:
				if word != string:
					if string.isdigit() and word.isdigit():
						result_list.append(tuple([NumericDistance(int(string),int(word)),word,0]))
					elif string.isalpha() and word.isdigit() or string.isdigit() and word.isalpha():
						pass
					elif string.isalnum() and word.isalnum():
						if not word[0] == string[0]:
							result_list.append(tuple([EditDistance(string,word),word,len(word)]))
						else:
							result_list.append(tuple([EditDistance(string,word),word,0]))
			result_dict.update({string : sorted(result_list, key=operator.itemgetter(0,2))[:closest_num]})
		print_output(result_dict, closest_num)

main()

